package ro.altom.altunitytester.altUnityTesterExceptions;

public class UnknownErrorException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -8975955604666095153L;

    public UnknownErrorException() {
    }

    public UnknownErrorException(String message) {
        super(message);
    }
}
