package ro.altom.altunitytester.altUnityTesterExceptions;

public class MethodNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -7517638165875804101L;

    public MethodNotFoundException() {
    }

    public MethodNotFoundException(String message) {
        super(message);
    }
}
